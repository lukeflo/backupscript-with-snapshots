# Backupscript Using Snapshots

This repo contains my personal backup-script inspired by the version from the [ArchWiki](https://wiki.archlinux.org/title/Rsync#Snapshot_backup).[^1]

It uses `rsync` and creates multiple incremental snapshots. Furthermore, I added a workaround to keep only a limited number of snapshots.

All further descriptions can be found as comments inside the script.

## Installation

After downloading/cloning the script-file you have to opy it to a directory included in your `$PATH`, e.g. from the directory containing the script:

```bash
$ cp backupscript ~/.local/bin/
```

You may have to replace `~/.local/bin/` with another destination included in your `$PATH` depending on your OS.

Afterwards, change to the directory and make the script executable:

```bash
$ cd ~/.local/bin
$ chmod 755 backupscript
```

If you want to use a backup disk with a fixed mount point, *which I highly recommend*, you may have to edit your `fstab`. See the particular [documentation](https://man.archlinux.org/man/fstab.5) for further explanations. There also exist many online tutorials how to change the `fstab` and include a disk manually.

## Usage

For backing up your system, home-directory or whatever defined folder just insert your backup drive and run the script from the terminal:

```bash
$ backupscript
```

The first backup may take some time depending on how big your data is. Following backups should be much faster thanks to the incremental option of `rsync`.

For me, the script works fine since nearly a year. But I take absolutely no warranty that it will work without problems for anybody else. *I highly recommend to make some test runs after you've adjusted the script to your needs!*

I've tried and use it only on my personal laptop running Arch-Linux. It should work on other GNU/Linux-distributions, but I haven't tested it. It may also run on MacOS with some minor changes, but, again, no warranty.

---

[^1]: Content of *ArchWiki* is available under [GNU Free Documentation License 1.3](https://www.gnu.org/copyleft/fdl.html) or later. 